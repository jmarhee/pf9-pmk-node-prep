resource "packet_reserved_ip_block" "pf9_pmk_virtualip" {
  project_id = "${var.project_id}"
  type       = "global_ipv4"
  quantity   = 1
  facility   = ""
}

data "template_file" "pf9_node" {
  template = "${file("${path.module}/userdata.tpl")}"

  vars = {
    pf9_du_url   = "${var.pf9_du_url}"
    pf9_username = "${var.pf9_username}"
    pf9_password = "${var.pf9_password}"
  }
}

data "template_file" "pf9_node_bgp" {
  template = "${file("${path.module}/userdata-bgp.tpl")}"

  vars = {
    pf9_du_url   = "${var.pf9_du_url}"
    pf9_username = "${var.pf9_username}"
    pf9_password = "${var.pf9_password}"
    anycast_ip = "${packet_reserved_ip_block.pf9_pmk_virtualip.address}"
  }
}

resource "packet_device" "pf9_node" {
  hostname         = "${format("pf9-node-%02d", count.index)}"
  operating_system = "ubuntu_16_04"
  plan             = "${var.plan_primary}"
  facilities       = ["${var.facility}"]
  tags             = ["pf9"]

  user_data = "${data.template_file.pf9_node.rendered}"

  count = "${var.node_count}"

  billing_cycle = "hourly"
  project_id    = "${var.project_id}"
}

resource "packet_device" "pf9_master" {
  hostname         = "${format("pf9-master-%02d", count.index)}"
  operating_system = "ubuntu_16_04"
  plan             = "${var.plan_primary}"
  facilities       = ["${var.facility}"]
  tags             = ["pf9", "pf9-pmk-master"]

  user_data = "${data.template_file.pf9_node_bgp.rendered}"

  count = "3"

  provisioner "local-exec" {
    command = "scp -i ${var.ssh_private_key_path} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null scripts/create_bird_conf.sh root@${self.access_public_ipv4}:/root/create_bird_conf.sh"
  }

  billing_cycle = "hourly"
  project_id    = "${var.project_id}"
}

resource "packet_bgp_session" "pf9_vip_0" {
  device_id      = "${packet_device.pf9_master.0.id}"
  address_family = "ipv4"
}

resource "packet_bgp_session" "pf9_vip_1" {
  device_id      = "${packet_device.pf9_master.1.id}"
  address_family = "ipv4"
}

resource "packet_bgp_session" "pf9_vip_2" {
  device_id      = "${packet_device.pf9_master.2.id}"
  address_family = "ipv4"
}
