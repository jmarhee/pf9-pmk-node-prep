output "prepared-node-ip-addresses" {
  description = "Your PMK-ready nodes, login to the Platform9 control plane (or using `pf9ctl`) to create a cluster and attach these nodes as Workers"
  value       = "\n${join("\n", packet_device.pf9_node.*.access_public_ipv4)}"
}

output "master-node-ip-addresses" {
  description = "Your PMK-ready Master Nodes login to the Platform9 control plane (or using `pf9ctl`) to create a cluster and attach these nodes as Masters"
  value       = "\n${join("\n", packet_device.pf9_master.*.access_public_ipv4)}"
}

output "virtual-ip-addresses" {
  description = "Create your cluster using this Virtual IP Address: "
  value = "${packet_reserved_ip_block.pf9_pmk_virtualip.address}"
}
