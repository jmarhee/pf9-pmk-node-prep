variable "auth_token" {
  description = "Your Packet API key"
}

variable "facility" {
  description = "Packet Facility"
  default     = "ewr1"
}

variable "project_id" {
  description = "Packet Project ID"
}

variable "plan_primary" {
  description = "Size of nodes (Defaults to x86 - baremetal_0)"
  default     = "t1.small.x86"
}

variable "node_count" {
  default     = "3"
  description = "Number of x86 worker nodes."
}

variable "pf9_du_url" {
  description = "URL for your Platform9 DU (i.e. https://some-id.platform9.io)"
}

variable "pf9_username" {
  description = "Platform9 DU Username"
}

variable "pf9_password" {
  description = "Platform9 DU Password"
}

variable "ssh_private_key_path" {
  description = "Path to SSH private key (only used locally)"
  default = "~/.ssh/id_rsa"
}
