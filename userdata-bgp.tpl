#!/bin/bash

export LC_ALL=C.UTF-8
export LANG=C.UTF-8

function start_anycast {
        apt update; apt install -y bird ; \
        while true; do \
                if [ ! -f /root/create_bird_conf.sh ]; then \
                        echo "Bird not ready...waiting..."
                else
                        bash /root/create_bird_conf.sh "${anycast_ip}"
                        break
                fi
        done
}

pf9_hostname="$(echo hostname)" sed -i "s/localhost/$pf9_hostname/g" /etc/hosts

#curl -O https://raw.githubusercontent.com/platform9/express-cli/master/cli-setup.sh
#echo -ne "${pf9_du_url}\n${pf9_username}\n${pf9_password}" | bash ./cli-setup.sh
curl -O https://raw.githubusercontent.com/jmarhee/express-cli/cli-setup-automation-friendly/cli-setup.sh
MGMTURL="${pf9_du_url}" USER="${pf9_username}" PASS="${pf9_password}" bash ./cli-setup.sh
echo -ne "y\n" | pf9ctl cluster prep-node

start_anycast
